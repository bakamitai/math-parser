import random, subprocess, sys
# Make the error strings less ugly. Maybe use join method?
# put negative numbers in parentheses

args_len = 23 if len(sys.argv) < 2 else int(sys.argv[1])
if ((args_len - 3) % 2) == 1:
    args_len += 1
    
operators = ["+", "*", "-", "/"]
num_tests = 100
num_passed = 0
error_log = "error_log.txt"

with open(error_log, "w") as err:
    for _ in range(num_tests):
        equation = ""

        open_parens = 0
        for i in range(args_len):
            if i & 1:
                index = random.randint(0, len(operators) - 1)
                equation += operators[index]
            else:
                n = random.randint(-4, 4)
                n = n if n != 0 else n + 1
                if random.random() < 0.3:
                    if random.random() < 0.5:
                        equation += '('
                        open_parens += 1
                        equation += str(n)
                    elif open_parens > 0:
                        equation += str(n)
                        equation += ')'
                        open_parens -= 1
                    else:
                        equation += str(n)
                else:
                    equation += str(n)

        equation += ')' * open_parens # python is fucked in the head for this
        equation += '\n'
        try:
            correct_result = eval(equation);#.replace("^", "**")
        except ZeroDivisionError as e:
            mp_output = subprocess.run("./mp", input=equation, capture_output=True, encoding="utf-8")
            last_line = mp_output.stderr.splitlines()[-1]
            if last_line == "Attempted divide by zero!":
                num_passed += 1
            else:
                print(f"Equation: {equation}\nstdout {mp_output.stdout}\nUncaught divide by zero")
            continue
        mp_output = subprocess.run("./mp", input=equation, capture_output=True, encoding="utf-8")
        if len(mp_output.stdout) == 0:
            print(
f'''No standard out
Eqation: {equation}
Standard Error: {mp_output.stderr}''', file=err)

        try:
            mp_result = float(mp_output.stdout)
            delta = abs(correct_result - mp_result)
        except OverflowError as e:
            print(f"{e}\nequation = {equation}\npython_result={correct_result}\nmp_result={mp_result}")
            continue
        except ValueError as v:
            print(f"{v}\nequation = {equation}\nstdout = {mp_output.stdout}\npython result = {correct_result}")
            continue
        
        if(delta > 0.0001):
            print(
f'''Incorrect result
Equation: {equation}
Correct answer = {correct_result}
mp answer = {mp_result}
Standard Error: {mp_output.stderr} ''', file=err)
        else:
            num_passed += 1

print(f"{num_passed}/{num_tests} tests passed!")
if num_passed != num_tests:
    print(f"Check {error_log} for details.")
