#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

// ULTIMATE TODO: The thing I must do before I can consider this project complete is prove that the algorithm works
//                with my epic Art Of Computer Programming knowledge.

// DONE: Focus on figuring out the '-' problem without transforming every '-' into a '+' even though that's funny ha ha.
//       I actually did it. The algorithm correctly handles all of the planned operators. Epic Style.

// DONE: My funny little trick with minus doesn't work anymore with exponentiation. Fix her!!!

// DONE: Error messages for invalid input please.
//       Need to check for valid parens babe

// DONE: Handle divide by zero situation please.
//       I have done this. Epic Style

// DONE: Add exponentiation to this sweet little girl.
//       Exponentiation has higher precedence than itself.

// DONE: Add parantheses to this hot little broad.
//       Actually easier than I thought. Although of course I had some sexy
//       little kinks to work out along the way.

// DONE: Make tree resizable hunty.
//       Easy Peasy

// DONE: grow_tree probably doesn't work anymore with the new add_node behaivour LOL
//       I was holding on to the past...

// DONE: Rewrite error function so that it doesn't rely on a TreeNode to be able to point to where
//       the error is.

// DONE: Don't exit program when error is called. Just print the error message and wait for new input.
//       Look at 14th character in input.txt remember what this means or I'll kill you. Debugger. "AA real monsters"
//       It appears to be working my beautiful boy

// TODO: Refactor everything.

// DONE: Add conditional compilation. Have debug information only be printed in debug mode babe.

// TODO: Figure out the EOF '\n' thing babe. You know the one ;P
//       I don't know if it even matters hunty but I don't know what's happening hunty

typedef struct TreeNode {
    enum {
        Number = 1,
        Operator,
    } type;

    union {
        double number;
        enum {
            Plus = 0,
            Minus,
            Multiply,
            Divide,
            Exponent,
        } operator;
    } value;

    uint8_t paren;
    uint64_t src_offset;
    int64_t index;
    int64_t left;
    int64_t right;
} TreeNode;

typedef struct {
    int64_t head;
} Tree;

/* #define buffer_len 4096 */
#define buffer_len 10
#define tree_capacity 64

#define is_operator(c) (((c) == '+') || ((c) == '^') || ((c) == '*') || ((c) == '/') || ((c) == '-'))

static char *operator_strings[] = {"Plus", "Minus", "Multiply", "Divide", "Exponent"};
static TreeNode *nodes;
static uint64_t  nodes_capacity;
static uint64_t nodes_length;
static char buffer[buffer_len] = {0};

void print_tree(Tree tree);
void print_tree_recursive(char *info, TreeNode *node, int depth);

void init_nodes(uint64_t size) {
    assert(size > 0);
    nodes = malloc(size * sizeof(TreeNode));
    nodes_length = 0;
    nodes_capacity = size;
}

void clear_nodes() {
    assert(nodes);
    assert(nodes_capacity > 0);
    memset(nodes, 0, nodes_capacity * sizeof(TreeNode));
    nodes_length = 0;
}

TreeNode *allocate_node(TreeNode node) {
    assert(nodes);
    assert(nodes_capacity > 0);
    if(nodes_length >= nodes_capacity) {
        nodes_capacity *= 2;
        nodes = realloc(nodes, nodes_capacity * sizeof(TreeNode));
    }

    node.index = nodes_length++;
    nodes[node.index] = node;
    return nodes + node.index;
}

TreeNode *get_left(TreeNode *node) {
    assert(nodes);
    assert(nodes_capacity > 0);
    if(node->left < 0) return NULL;
    assert(node->left < (int64_t) nodes_length);
    return nodes + node->left;
}

TreeNode *get_right(TreeNode *node) {
    assert(nodes);
    assert(nodes_capacity > 0);
    if(node->right < 0) return NULL;
    assert(node->right < (int64_t) nodes_length);
    return nodes + node->right;
}

TreeNode *get(uint64_t index) {
    assert(nodes);
    assert(nodes_capacity > 0);
    assert(index < nodes_length);
    return nodes + index;
}

void error(uint64_t src_offset, const char *format, ...) {
    va_list args;
    fprintf(stderr, "%s", buffer);

    for(uint64_t i = 0; i < src_offset; i++) {
        fputc(' ', stderr);
    }
    fputc('^', stderr);
    fputc('\n', stderr);

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
}

int get_precedence(TreeNode *node) {
    if(node->type == Number || node->paren) return 999;
    return node->value.operator / 2;
}

void print_tree_recursive(char *info, TreeNode *node, int depth) {
    for(int i = 0; i < depth; i++) {
        fputc(' ', stderr);
        fputc(' ', stderr);
    }
    fprintf(stderr, "%s index(%lu): ", info, node->index);
    if(node->type == Operator)
        fprintf(stderr, "Operator: %s\n", operator_strings[node->value.operator]);
    else
        fprintf(stderr, "Number: %lf\n", node->value.number);

    TreeNode *left = get_left(node);
    TreeNode *right = get_right(node);
    if(left) print_tree_recursive("left child", left, depth + 1);
    if(right) print_tree_recursive("right child", right, depth + 1);
}

void print_tree(Tree tree) {
    print_tree_recursive("head", get(tree.head), 0);
}

void print_node(TreeNode *node) {
    if(!node) fprintf(stderr, "NULL\n");
    else if(node->type == Operator)
        fprintf(stderr, "Operator: %s\n", operator_strings[node->value.operator]);
    else
        fprintf(stderr, "Number: %lf\n", node->value.number);
}

double solve_recursive(TreeNode *node) {
    if(node->type == Number) return node->value.number;

    switch(node->value.operator) {
    case Plus: return solve_recursive(get_left(node)) + solve_recursive(get_right(node));
    case Minus: return solve_recursive(get_left(node)) - solve_recursive(get_right(node));
    case Divide: {
        double lhs = solve_recursive(get_left(node));
        double rhs = solve_recursive(get_right(node));
        if(rhs == 0.0) {
            error(node->src_offset, "Attempted divide by zero!\n");
            return 0;
        }
        return lhs / rhs;
    }
    case Multiply: return solve_recursive(get_left(node)) * solve_recursive(get_right(node));
    case Exponent: return pow(solve_recursive(get_left(node)), solve_recursive(get_right(node)));
    }
    fprintf(stderr, "Unreachable\n");
    exit(1);
    return 0.0;
}

double solve(Tree tree) {
    return solve_recursive(get(tree.head));
}

void check_node(TreeNode *node) {
#ifndef NDEBUG
    static int expecting_number = 1;
    if(node == NULL) {
        expecting_number = 1;
        return;
    }

    if(expecting_number) {
        assert(node->type == Number || node->paren);
        /* if(node->type == Operator && !node->paren) { */
        /*     error(node->src_offset, "Expected a number. Operator %s found\n", */
        /*           operator_strings[node->value.operator]); */

        /*     return 0; */
        /* } */
    }
    else {
        assert(node->type == Operator);
        /* if(node->type == Number) { */
        /*     error(node->src_offset, "Expecting an operator. Number %lf found\n", */
        /*           node->value.number); */
        /*     return 0; */
        /* } */
    }
    expecting_number = !expecting_number;
#endif
}

void steal_right(TreeNode *current, TreeNode *new) {
    TreeNode *current_right = get_right(current);
    if(current_right->type == Number) {
        new->left = current_right->index;
        current->right = new->index;
        return;
    }

    int right_precedence = get_precedence(current_right);
    int new_precedence = get_precedence(new);
    if(new_precedence <= right_precedence) {
        // Exponent has highest precedence. If it is here then precedence is equal.
        if(new->value.operator == Exponent) {
            steal_right(current_right, new);
        }
        else {
            new->left = current_right->index;
            current->right = new->index;
        }
    }
    else {
        steal_right(current_right, new);
    }
}

int add_node(Tree *tree, TreeNode *new) {
    assert(tree);

    check_node(new);
    // Check if tree has a head. Set it if not.
    if(tree->head == -1) {
        tree->head = new->index;
        return 1;
    }

    TreeNode *current = get(tree->head);
    // Number is some operators right child. Find the operator
    if(new->type == Number || new->paren) {
        TreeNode *current_right = NULL;
        for(;;) {
            current_right = get_right(current);
            if(!current_right) {
                current->right = new->index;
                return 1;
            }
            current = current_right;
        }
    }
    // Operator has a left child. Find the child
    else {
        // This can only be true if new is the first operator added to the tree
        TreeNode *head = get(tree->head);
        if(head->type == Number || head->paren) {
            new->left = head->index;
            tree->head = new->index;
            return 1;
        }
        // Find left child obeying precedence rules
        else {
            int new_precedence = get_precedence(new);
            int current_precedence = get_precedence(current);
            if(new_precedence <= current_precedence) {
                // Exponent has highest precedence. If it is here then precedence is equal.
                if(new->value.operator == Exponent) {
                    steal_right(current, new);
                }
                else {
                    new->left = current->index;
                    tree->head = new->index;
                }
            }
            else {
                steal_right(current, new);
            }
        }
    }
    // Can she even get here? Yes
    return 1;
}

char *parse_number(Tree *tree, char *location) {
    TreeNode node = {0};
    node.left = -1;
    node.right = -1;
    node.type = Number;
    node.src_offset = (uint64_t) (location - buffer);
    node.value.number = strtod(location, &location);
    if(errno == ERANGE) {
        error(node.src_offset, "Trying to read number caused overflow.\n");
        return NULL;
    }

    if(!add_node(tree, allocate_node(node))) return NULL;
    return location;
}

char *parse_operator(Tree *tree, char *location) {
    TreeNode node = {0};
    node.left = -1;
    node.right = -1;
    node.src_offset = (uint64_t) (location - buffer);
    switch(*location) {
    case '+':
        node.type = Operator;
        node.value.operator = Plus;
        break;

    case '-':
        node.type = Operator;
        node.value.operator = Minus;
        break;

    case '/':
        node.type = Operator;
        node.value.operator = Divide;
        break;

    case '*':
        node.type = Operator;
        node.value.operator = Multiply;
        break;

    case '^':
        node.type = Operator;
        node.value.operator = Exponent;
        break;

    default:
        error(node.src_offset, "This aint a fucken operatah %c\n", *location);
        return NULL;
        break;
    }
    if(!add_node(tree, allocate_node(node))) return NULL;
    return location + 1;
}

static char previous_non_whitespace_character = '\0';
char *parse_tree(Tree *tree, char *location) {

    while(*location != '\n' && *location != '\0') {
        if(isspace(*location)) {
            location++;
            continue;
        }

        if(*location == '-') { // '-' could be a minus sign or a prefix to a negative number 
            if(is_operator(previous_non_whitespace_character) || previous_non_whitespace_character == '\0') {
                previous_non_whitespace_character = -1;
                location = parse_number(tree, location);
                if(!location) return NULL;
            }
            else {
                previous_non_whitespace_character = *location;
                location = parse_operator(tree, location);
                if(!location) return NULL;
            }
        }
        else if(is_operator(*location)) {
            previous_non_whitespace_character = *location;
            location = parse_operator(tree, location);   
            if(!location) return NULL;
        }
        else if(*location == '(') {
            location++;
            Tree paren_subtree = {-1};
            location = parse_tree(&paren_subtree, location);
            if(!location) return NULL;
            get(paren_subtree.head)->paren = 1;
            check_node(NULL); // Last node in subree was a number and we are treating this operator as a number too.
            if(!add_node(tree, get(paren_subtree.head))) return NULL;
        }
        else if(*location == ')') {
            return location + 1;
        }
        else {
            previous_non_whitespace_character = -1;
            location = parse_number(tree, location);
            if(!location) return NULL;
        }
    }
    return location;
}

char *number_operator_order(char *input) {
    int expecting_number = 1;
    int expecting_operator = 0;

    while(*input != '\n' && *input != ')' && *input != '\0') {
        if(isspace(*input)) {
            input++;
            continue;
        }

        if(*input == '-' || (*input >= '0' && *input <= '9')) {
            if(expecting_operator) {
                if(*input == '-') {
                    input++;
                    expecting_number = !expecting_number;
                    expecting_operator = !expecting_operator;
                    continue;
                }
                else {
                    error((uint64_t) (input - buffer), "Expecting operator, number found!\n");   
                    return NULL;
                }
            }
            strtod(input, &input);
            expecting_number = !expecting_number;
            expecting_operator = !expecting_operator;
        }
        else if(is_operator(*input)) {
            if(expecting_number)  {
                error((uint64_t) (input - buffer), "Expecting number, operator found!\n");
                return NULL;
            }
            input++;
            expecting_number = !expecting_number;
            expecting_operator = !expecting_operator;
        }
        else if(*input == '(') {
            if(expecting_operator) {
                error((uint64_t) (input - buffer), "Expecting operator, open paren found!\n");
                return NULL;
            }
            input++;
            input = number_operator_order(input) + 1;
            expecting_number = !expecting_number;
            expecting_operator = !expecting_operator;
        }
    }
    return input;
}

int validate_input(char *input) {
    size_t length = strlen(input);

    // count parentheses
    int num_parens = 0;
    int last_open_paren_index = 0;
    for(size_t i = 0; i < length; i++) {
        if(input[i] == '(') {
            num_parens += 1;
            last_open_paren_index = i;
        }
        else if(input[i] == ')') {
            if(num_parens == 0) {
                error(i, "Unbalanced Parentheses:\n\t Closing paren with no opening paren!\n");
                return 0;
            }
            num_parens -= 1;
        }
    }

    // num_parens should be 0
    if(num_parens) {
        error(last_open_paren_index, "Unbalanced Parentheses:\n\tOpen paren with no closing paren!\n");
        return 0;
    }

    // Check number operator order is correct
    if(!number_operator_order(input)) return 0;
    return 1;
}

int main() {
    Tree tree = {0};
    tree.head = -1;
    init_nodes(tree_capacity);

    while(fgets(buffer, buffer_len, stdin) != NULL) {
        size_t equation_length = strlen(buffer);
        if(buffer[equation_length - 1] != '\n') {
            error(0, "Equation is invalid. %lu is the len. %d is the char\n", buffer_len, buffer[equation_length]);
            int c, eekum_bokum = 0;
            while((c = fgetc(stdin)) != '\n' && c != EOF)
                eekum_bokum++;
            if(c == EOF) return 0;
            continue;
        }
        // Reset expecting number
        check_node(NULL);
        tree.head = -1;
        clear_nodes();

        if(!validate_input(buffer)) continue;
        if(!parse_tree(&tree, buffer)) continue;

#ifndef NDEBUG
        print_tree(tree);
#endif
        printf("%lf\n", solve(tree));
    }
    free(nodes);
    return 0;
}
