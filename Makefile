mp : math_parser.c
	cc -g -Wall -Wextra math_parser.c -lm -o mp

release : math_parser.c
	cc -O2 math_parser.c -lm -DNDEBUG -o mp

run : mp
	./mp
